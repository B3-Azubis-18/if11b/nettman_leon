CREATE TABLE IF NOT EXISTS `nettmann`.`users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`));
CREATE TABLE IF NOT EXISTS `nettmann`.`bestellungen` (
  `bestell_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `klasse` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ausstattung` varchar(255) COLLATE utf8_unicode_ci,
  `abholort` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bestell_id`));
