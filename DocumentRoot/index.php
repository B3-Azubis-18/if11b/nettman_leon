<?php
        session_start();
?>

<html>
    <head>
        <title>Autohaus Nettmann</title>
    </head>
    <body>

    <img src="./img/logo_Nettmann.png" alt="Logo Nettmann" width="150px" align="right">

    <?php
        require_once("modules/db.php");
        require_once("modules/login.php");
        $login = new Login();

        if ($login->LoggedIn() == true) {
            include("modules/form.php");
        } else {
            include("modules/loggedout.php");
        }
    ?>
        <!-- Beginn Fußzeile -->
        <!-- ############### -->
        <hr color="blue">
        <table width:"100%" >
        <tr align="left">
            <th>
            Kontakt:
            </th>
            <th>
            Bankdaten:
            </th>
        </tr>
        <tr align="left" >
            <td style="padding-left: 20px">
            <ul>
                <li>Ottostraße 22, 90762 Fürth</li>
                <li>0911/11…</li>
                <li>info@autohaus-nettmann.de</li>
            </ul>
            </td>
            <td style="padding-left: 20px">
            <ul>
                <li>IBAN: DE76 1231 …</li>
                <li>BIC: 123 …</li>
                <li>Institut: SuperBank</li>
            </ul>
            </td>
        </tr>
        </table>
    </body>
</html>