<?php
class CreateUser
{
    private $db = null;
    public function __construct()
    {
        if (isset($_POST["create"])) {
            $this->NewUser();
        }
    }
    private function NewUser()
    {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if (!$this->db->connect_errno) {
            $user_name = $_POST['username'];
            $user_password = $_POST['password'];
            $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT);
            $sql = "INSERT INTO users (user_name, user_password_hash)
                    VALUES('" . $user_name . "', '" . $user_password_hash . "');";
            $output = $this->db->query($sql);
            if ($output) {
                echo "Created";
            } else {
                echo "Error";
            }
        } else {
            echo "Datenbank nicht erreichbar";
        }
    }
}
