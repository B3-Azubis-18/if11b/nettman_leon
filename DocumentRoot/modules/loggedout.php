<form method="post" action="index.php" name="loginform">

    <label for="input_username">Benutzername</label>
    <input id="input_username" type="text" name="username" required />
    <label for="input_password" style="padding-left: 15px" >Passwort</label>
    <input id="input_password" type="password" name="user_password" required />

    <input type="submit" name="login" value="Anmelden" style="margin-left: 15px"/>

</form>

<?php
if (isset($login)) {
    if ($login->warnings) {
        foreach ($login->warnings as $warning) {
            echo $warning;
        }
    }
}
?>