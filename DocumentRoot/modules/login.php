<?php
class Login
{
    private $db = null;
    public $warnings = array();

    //Konstruktor
    public function __construct()
    {
        if (isset($_GET["logout"])) {
            $this->Logout();
        }
        elseif (isset($_POST["login"])) {
            $this->loguserin();
        }
    }

    private function loguserin()
    {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if (!$this->db->connect_errno) {

            // https://www.w3schools.com/php/func_mysqli_real_escape_string.asp
            $username = $this->db->real_escape_string($_POST['username']);
            $sql = "SELECT user_name, user_password_hash
                    FROM users
                    WHERE user_name = '" . $username . "';";
            $result = $this->db->query($sql);
            if ($result->num_rows == 1){
                $output = $result->fetch_object();
                if (password_verify($_POST['user_password'], $output->user_password_hash)) {

                    $_SESSION['username'] = $output->user_name;
                    $_SESSION['loggedin'] = 1;
                }
            } else {
                $this->warnings[] = "Falscher Benutzername oder Passwort!";
            }
        } else {
            $this->warnings[] = "Datenbank nicht erreichbar";
        }
    }

    public function Logout()
    {
        $_SESSION = array();
        session_destroy();
        $this->warnings[] = "You have been logged out.";

    }

    public function LoggedIn()
    {
        if (isset($_SESSION['loggedin']))
        {
            if($_SESSION['loggedin'] == 1) {
                return true;
            }
        }
        return false;
    }
}